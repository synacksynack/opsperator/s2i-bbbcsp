FRONTNAME = opsperator
-include Makefile.cust

PHONY: testcontainer
testcontainer:
	docker rm -f testcsp || echo mkay
	docker run --name testcsp \
	    --user 0 \
	    -v /var/run/docker.sock:/var/run/docker.sock \
	    -e API_HOST=csp.demo.local \
	    -e API_PORT=8080 \
	    -e CONTAINERS_DRIVER=docker \
	    -e "BIGBLUEBUTTON_API_URL=https://demo2.bigbluebutton.org/bigbluebutton/api" \
	    -e "BIGBLUEBUTTON_API_SECRET=invalid" \
	    -e "PEERTUBE_API_URL=https://peertube.cpy.re" \
	    -e "PEERTUBE_USERNAME=doesnotexist" \
	    -e "PEERTUBE_PASSWORD=mysecret" \
	    -e "SERVE_STATIC_ASSETS=yay" \
	    -it opsperator/bbbcsp

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task-build task-scan pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in configmap secret deployment service; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-; \
	BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "BBBCSP_REPOSITORY_REF=$$BRANCH" \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "BBBCSP_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-persistent.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: prep-runtime
prep-runtime:
	if test "$$API_HOST"; then \
	    test -z "$$API_PROTO" && API_PROTO=https; \
	    if test -z "$$API_PORT"; then \
		if test "$$API_PROTO" = https; then \
		    API_PORT=443; \
		else \
		    API_PORT=80; \
		fi; \
	    fi; \
	    echo "var API_HOST = '$$API_HOST';" >./static/js/backend.js; \
	    echo "var API_PORT = '$$API_PORT';" >>./static/js/backend.js; \
	    echo "var API_PROTO = '$$API_PROTO';" >>./static/js/backend.js; \
	elif ! test -s ./static/js/backend.js; then \
	    cat ./static/js/backend.js.sample >./static/js/backend.js; \
	fi
	cat ./static/js/backend.js

.PHONY: run
run: prep-runtime
	node ./workers/index.js

.PHONY: run-pm2
run-pm2: prep-runtime
	pm2 start ecosystem.config.js --no-daemon --no-vizion;

.PHONY: start
start: run
