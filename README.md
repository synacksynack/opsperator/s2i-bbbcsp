# BigBlueButton Conference Streaming Platform

Starts and Stops BigBlueButton LiveStream containers.

Based on https://github.com/Worteks/bbb-csp
